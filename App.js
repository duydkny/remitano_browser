import React, {useState} from 'react';

import {WebView as RNWebView} from 'react-native-webview';
import {
  RefreshControl,
  ScrollView,
  SafeAreaView,
  View,
  ActivityIndicator,
  TouchableOpacity,
  Text,
  TextInput,
  Dimensions,
  StyleSheet,
  Platform,
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import * as Progress from 'react-native-progress';

/* Inject Javascript function to tell native webview when web content scrolled to top */
const INJECTED_JS = `
  window.onscroll = function() {
    window.ReactNativeWebView.postMessage(
      JSON.stringify({
        scrollTop: Math.max(document.documentElement.scrollTop, document.body.scrollTop)
      }),     
    )
  }

  var scrollElement = document.getElementById('scrollable-body');
  scrollElement.onscroll = function() {
    window.ReactNativeWebView.postMessage(
      JSON.stringify({
        scrollTop: scrollElement.scrollTop
      }),     
    )
  }
`;

const webViewRef = React.createRef();
const windowWidth = Dimensions.get('window').width;

const webViewStyles = (height) => ({
  height,
});

const App = () => {
  const [isPullToRefreshEnabled, setPullToRefreshEnabled] = useState(true);
  const [scrollViewHeight, setScrollViewHeight] = useState(0);
  const [inputUrlFocus, setInputUrlFocus] = useState(false);
  const [inputValue, setInputValue] = useState('remitano.com');
  const [isOnHomePage, setOnHomePage] = useState(true);
  const [url, setUrl] = useState('');
  const [isLoading, setLoading] = useState(false);
  const [canGoBack, setCanGoBack] = useState(false);
  const [canGoForward, setCanGoForward] = useState(false);
  const [progress, setProgress] = useState(0);

  const onRefresh = () => {
    if (webViewRef.current) {
      webViewRef.current.reload();
    }
  };

  /* Receive webview content scrolled to top event, then enable pull to refresh */
  const onWebViewMessage = (e) => {
    const {data} = e.nativeEvent;
    try {
      console.log(data);
      const {scrollTop} = JSON.parse(data);
      setPullToRefreshEnabled(scrollTop === 0);
    } catch (error) {
      console.log(error);
    }
  };

  const renderLoading = () => {
    return <ActivityIndicator color="purple" />;
  };

  /* Fire when user finish typing website url and press enter */
  const onEnter = (e) => {
    const text = e.nativeEvent.text.toLowerCase();
    if (!text.startsWith('http')) {
      const url = 'http://' + text;
      setInputValue(url.toLowerCase());
      setUrl(url.toLowerCase());
    } else {
      setInputValue(text.toLowerCase());
      setUrl(text.toLowerCase());
    }
    setOnHomePage(false);
  };

  const onChangeText = (text) => {
    setInputValue(text);
  };

  /* Go to home page, fire when user click home button */
  const goToHomePage = () => {
    setOnHomePage(true);
    setUrl('');
    setInputValue('');
    setLoading(false);
    setCanGoForward(false);
    setCanGoBack(false);
  };

  /* Fire when user click refresh button */
  const onRefreshClick = () => {
    if (!isOnHomePage) {
      if (webViewRef.current) {
        webViewRef.current.reload();
      }
    }
  };

  /* Fire when user click back button */
  const onBackClick = () => {
    if (webViewRef.current) {
      webViewRef.current.goBack();
    }
  };

  /* Fire when user click forward button */
  const onForwardClick = () => {
    if (webViewRef.current) {
      webViewRef.current.goForward();
    }
  };

  /* Fire when webview finished loading */
  const onLoadEnd = (e) => {
    setLoading(false);
    const {canGoBack} = e.nativeEvent;
    const {canGoForward} = e.nativeEvent;
    setCanGoBack(canGoBack);
    setCanGoForward(canGoForward);
    const {url} = e.nativeEvent;
    setInputValue(url);
  };

  /* Fire when webview loading and return progress */
  const onProgress = (e) => {
    const {progress} = e.nativeEvent;
    setProgress(progress);
  };

  /* Fire when webview start loading */
  const onLoadStart = (e) => {
    const {loading} = e.nativeEvent;
    setLoading(loading);
  };

  /* Fire when user click stop button */
  const onStopClick = () => {
    if (webViewRef.current) {
      webViewRef.current.stopLoading();
      setLoading(false);
    }
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <View style={styles.toolbar}>
        <TouchableOpacity onPress={onBackClick}>
          <Text style={{marginHorizontal: 10}}>
            <Icon
              name="arrow-back-outline"
              size={30}
              color={canGoBack ? 'purple' : 'gray'}
            />
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={onForwardClick}>
          <Text style={{marginEnd: 10}}>
            <Icon
              name="arrow-forward-outline"
              size={30}
              color={canGoForward ? 'purple' : 'gray'}
            />
          </Text>
        </TouchableOpacity>

        {isLoading ? (
          <TouchableOpacity onPress={onStopClick}>
            <Text style={{marginEnd: 10}}>
              <Icon name="close-outline" size={35} color="purple" />
            </Text>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity onPress={onRefreshClick}>
            <Text style={{marginEnd: 10}}>
              <Icon
                name="refresh-outline"
                size={30}
                color={isOnHomePage ? 'gray' : 'purple'}
              />
            </Text>
          </TouchableOpacity>
        )}

        <TouchableOpacity onPress={goToHomePage}>
          <Text style={{marginEnd: 10}}>
            <Icon name="home-outline" size={24} color="purple" />
          </Text>
        </TouchableOpacity>
        <TextInput
          style={[
            styles.addressBar,
            {borderColor: inputUrlFocus ? 'purple' : 'gray'},
          ]}
          placeholder="Enter website url"
          onFocus={() => setInputUrlFocus(true)}
          onBlur={() => setInputUrlFocus(false)}
          onSubmitEditing={onEnter}
          value={inputValue}
          onChangeText={onChangeText}
          blurOnSubmit={true}
        />
      </View>
      {isOnHomePage ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontSize: 24, fontWeight: 'bold'}}>
            This is home page
          </Text>
        </View>
      ) : (
        <View style={{marginTop: 5, flex: 1}}>
          {isLoading ? (
            <Progress.Bar
              progress={progress}
              width={windowWidth}
              height={2}
              color="purple"
              borderWidth={0}
              unfilledColor="white"
            />
          ) : (
            <View style={{width: 100, height: 5}} />
          )}
          <ScrollView
            onLayout={(e) => setScrollViewHeight(e.nativeEvent.layout.height)}
            refreshControl={
              <RefreshControl
                refreshing={false}
                enabled={isPullToRefreshEnabled}
                onRefresh={onRefresh}
                tintColor="purple"
                colors={['purple']}
                style={{backgroundColor: 'transparent'}}
              />
            }>
            <RNWebView
              source={{uri: url}}
              ref={webViewRef}
              style={webViewStyles(scrollViewHeight)}
              onMessage={onWebViewMessage}
              injectedJavaScript={INJECTED_JS}
              startInLoadingState={true}
              renderLoading={renderLoading}
              onLoadStart={onLoadStart}
              onLoadEnd={onLoadEnd}
              onLoadProgress={onProgress}
            />
          </ScrollView>
        </View>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  toolbar: Platform.select({
    ios: {flexDirection: 'row', alignItems: 'center'},
    android: {flexDirection: 'row', alignItems: 'center', marginTop: 5},
  }),

  addressBar: {
    height: 35,
    flex: 1,
    borderWidth: 1,
    borderRadius: 20,
    maxHeight: 35,
    width: 100,
    marginEnd: 10,
    paddingHorizontal: 10,
  },
});

export default App;
